package cyapp.practical06;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.util.Scanner;

public class MyCreateSignature {
	@SuppressWarnings("restriction")
	public static void main(String args[]) {
		try {
			System.out.print("Enter name of your data file without extention: ");
			Scanner in = new Scanner(System.in);
			
			//generate key pair
			KeyPairGenerator kg = KeyPairGenerator.getInstance("DSA", "SUN");
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
			kg.initialize(1024, random);
			KeyPair key = kg.generateKeyPair();
			PrivateKey privateKey = key.getPrivate();
			PublicKey publicKey = key.getPublic();
			
			//get signature object
			Signature signature = Signature.getInstance("SHA1withDSA", "SUN");
			signature.initSign(privateKey);
			
			//update loop
			FileInputStream dataFis = new FileInputStream("Signature Files\\" + in.nextLine() + ".data");
			System.out.println("Creating Signature...");
			BufferedInputStream dataBis = new BufferedInputStream(dataFis);
			byte[] dataBuffer = new byte[1024];
			
			int length;
			
			while (dataBis.available() != 0) {
				length = dataBis.read(dataBuffer);
				signature.update(dataBuffer, 0, length);
			}
			dataBis.close();
			
			//create signature and signature file
			byte[] SignatureBytes = signature.sign();
			FileOutputStream signatureFos = new FileOutputStream("Signature Files\\mySignature.sig");
			//signatureFos.write(SignatureBytes);
			new sun.misc.BASE64Encoder().encode(SignatureBytes, signatureFos);
			signatureFos.close();
			System.out.println("Signature saved to mySignature.sig:");
			System.out.println(new sun.misc.BASE64Encoder().encode(SignatureBytes) + "\n");
			
			//create public key file
			byte[] encodedPublicKeyBytes = publicKey.getEncoded();
			FileOutputStream publicKeyFos = new FileOutputStream("Signature Files\\myPublicKey.pk");
			//publicKeyFos.write(encodedPublicKeyBytes);
			new sun.misc.BASE64Encoder().encode(encodedPublicKeyBytes, publicKeyFos);
			publicKeyFos.close();
			System.out.println("Public key saved to myPublicKey.pk:");
			System.out.println(new sun.misc.BASE64Encoder().encode(encodedPublicKeyBytes) + "\n");
			
			MainMenu.main(args);
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
