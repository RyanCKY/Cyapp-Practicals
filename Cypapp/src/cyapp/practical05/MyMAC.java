package cyapp.practical05;

import java.util.Scanner;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;

public class MyMAC {	
	public static void main(String[] args) {
		try {
			//get message
			Scanner in = new Scanner(System.in);
			System.out.print("Enter a string to encrypt: ");
			byte[] message = in.nextLine().getBytes("UTF8");
			
			//generate key
			KeyGenerator kg = KeyGenerator.getInstance("HmacSHA1");
			SecretKey key = kg.generateKey();
			
			//get MAC object
			Mac mac = Mac.getInstance("HmacSHA1");
			mac.init(key);
			mac.update(message);
			System.out.println("Provider: " + mac.getProvider().getInfo());
			
			//get HMAC
			byte[] hmac = mac.doFinal();
			
			//print results
			System.out.println("");
			System.out.println("MAC: HmacSHA1");
			System.out.println("Message: " + new String(message));
			System.out.println("HMAC: \"" + new String(hmac, "UTF8") + "\"");
			
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
