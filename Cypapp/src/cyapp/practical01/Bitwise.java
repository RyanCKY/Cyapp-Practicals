package cyapp.practical01;
import java.util.Scanner;

public class Bitwise {
	public static java.lang.String stringXorByte (java.lang.String str, byte b) {
		String returnStr = "";
		char ch;
		
		for (int i = 0 ; i < str.length() ; i++) {
			ch = str.charAt(i);
			ch ^= b;
			returnStr += ch;
		}
		return returnStr;
	}
	
	public static byte[] toByteArray (java.lang.String str) {
		if (str == null) {
			return null;
		}
		else if (str.length() < 2) {
			return null;
		}
		else {
			int len = str.length() / 2;
			byte [] buffer = new byte[len];
			
			for (int i = 0 ; i < len ; i++) {
				buffer[i] = (byte) Integer.parseInt(str.substring(i*2,i*2+2), 16);
			}
			return buffer;
		}
	}
	
	public static java.lang.String toHexString (byte[] bytes) {
		if (bytes == null) {
			return null;
		}
		else {
			int len = bytes.length;
			String str = "";
			for (int i = 0 ; i < len ; i++) {
				if ((bytes[i]&0xFF) < 16) {
					str += "0" + Integer.toHexString(bytes[i]&0xFF);
				}
				else {
					Integer.toHexString(bytes[i]&0xFF);
				}
			}
			return str.toUpperCase();
		}
	}
	
	
	public static void main (String[] args) {
		Scanner in = new Scanner (System.in);
		
		String input, output;
		byte bytes;
		
		System.out.print("Enter a word: ");
		input = in.nextLine();
		
		System.out.print("Enter a byte: ");
		bytes = in.nextByte();
		
		output = stringXorByte(input, bytes);
		
		System.out.print("Your encrypted word is: " + output);
		
		in.close();
	}
}
