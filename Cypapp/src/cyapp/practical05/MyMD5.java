package cyapp.practical05;

import java.security.MessageDigest;
import java.util.Scanner;

public class MyMD5 {
	public static void main (String[] args) {
		try {
			//get message
			Scanner in = new Scanner(System.in);
			System.out.print("Enter a string to encrypt: ");
			byte[] message = in.nextLine().getBytes("UTF8");
			
			//create message digest object for MD5
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			System.out.println("Provider: " + messageDigest.getProvider().getInfo());
			
			//create message digest
			messageDigest.update(message);
			byte[] md = messageDigest.digest();
			
			//print results
			System.out.println("Message Digest Algorithm: MD5");
			System.out.println("Message: " + new String(message));
			System.out.println("Message Digest: \"" + new String(md, "UTF8") + "\"");
			
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
