package cyapp.practical06;

import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class MainMenu {
	public static void main (String args[]) throws FileNotFoundException{
		Scanner in = new Scanner(System.in);
		
		int choice = 0;
		String message;
		
		System.out.println("=================================");
		System.out.println("|           Main Menu           |");
		System.out.println("=================================");
		System.out.println("|    1. Create a Signature      |");
		System.out.println("|    2. Verify a Signature      |");
		System.out.println("|    3. Create a Data File      |");
		System.out.println("|          4. Exit              |");
		System.out.println("=================================");
		
		if (choice > 0 || choice < 5) {
			System.out.print("Enter a selection: ");
			choice = in.nextInt();
		}
		else {
			System.out.println("ERROR! Please enter a correct selection!");
			System.out.print("Enter a selection: ");
			choice = in.nextInt();
		}
		
		in.nextLine();
		
		if (choice == 1) {
			MyCreateSignature.main(args);
		}
		else if (choice == 2) {
			MyVerifySignature.main(args);
		}
		else if (choice == 3) {
			PrintWriter out = new PrintWriter("Signature Files\\datafile.data");
			System.out.print("Enter text for data file: ");
			message = in.nextLine();
			out.print(message);
			out.close();
			MainMenu.main(args);
		}
		else if (choice == 4) {
			System.out.print("THE SYSTEM WILL NOW TERMINATE");
			System.exit(0);
		}
		
		in.close();
	}
}
