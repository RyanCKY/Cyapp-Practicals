package cyapp.practical04;

import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import java.util.Scanner;

public class MyDes {
	public static void main (String[] args) {
		try {
			//get plain text
			System.out.print("Enter a string to encrypt: ");
            Scanner in = new Scanner(System.in);
			byte [] plainText = in.next().getBytes("UTF8");
			
			//generate DES key
			KeyGenerator kg = KeyGenerator.getInstance("DES");
			Key key = kg.generateKey();
			
			//create cipher object for DES
			Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
			//Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
			System.out.println("Provider: " + cipher.getProvider().getInfo());
			
			//encrypt plain text
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] cipherText = cipher.doFinal(plainText);
			System.out.println("DES with ECB");
			System.out.println("Plain Text: " + new String(plainText, "UTF8"));
			System.out.println("Cipher text: " + new String(cipherText, "UTF8"));
			
			//decrypt cipher text
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] decryptedPlainText = cipher.doFinal(cipherText);
			System.out.println("Decrypted plain text: " + new String(decryptedPlainText, "UTF8"));
			
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
