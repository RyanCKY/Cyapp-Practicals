package cyapp.practical06;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.util.Scanner;

public class MyVerifySignature {
	@SuppressWarnings("restriction")
	public static void main (String args[]) {
		try {
			Scanner in = new Scanner(System.in);
			
			System.out.print("Enter name of Public Key File: ");
			String publickeyfilename = "Signature Files\\" + in.nextLine() + ".pk";
			
			System.out.print("Enter name of Signature File: ");
			String signaturefilename = "Signature Files\\" + in.nextLine() + ".sig";
			
			System.out.print("Enter name of Data File: ");
			String datafilename = "Signature Files\\" + in.nextLine() + ".data";
			
			System.out.println("");
			
			System.out.println("Signature verification in progress...\n");
			
			//import encoded public key
			FileInputStream publicKeyFis = new FileInputStream(publickeyfilename);
			//byte[] encodedPublicKeyBytes = new byte[publicKeyFis.available()];
			//publicKeyFis.read(encodedPublicKeyBytes);
			byte[] encodedPublicKeyBytes = new sun.misc.BASE64Decoder().decodeBuffer(publicKeyFis);
			publicKeyFis.close();
			
			//get public key
			X509EncodedKeySpec encodedPublicKeySpec = new X509EncodedKeySpec(encodedPublicKeyBytes);
			KeyFactory kf = KeyFactory.getInstance("DSA", "SUN");
			PublicKey publickey = kf.generatePublic(encodedPublicKeySpec);
			System.out.println("Extracted public key from " + publickeyfilename);
			
			//import signature file
			FileInputStream signatureFis = new FileInputStream(signaturefilename);
			//byte[] signaturebytes = new byte[signatureFis.available()];
			//signatureFis.read(signatureBytes);
			byte[] signatureBytes = new sun.misc.BASE64Decoder().decodeBuffer(signatureFis);
			signatureFis.close();
			System.out.println("Extracted signature from " + signaturefilename);
			
			//initialise signature object
			Signature signature = Signature.getInstance("SHA1withDSA", "SUN");
			signature.initVerify(publickey);
			
			//read data file
			FileInputStream dataFis = new FileInputStream(datafilename);
			BufferedInputStream dataBis = new BufferedInputStream(dataFis);
			byte[] dataBuffer = new byte[1024];
			int length;
			
			while (dataBis.available() != 0) {
				length = dataBis.read(dataBuffer);
				signature.update(dataBuffer, 0, length);
			}
			
			dataBis.close();
			System.out.println("Read data from " + datafilename + "\n");
			
			//verify signature
			System.out.println("Result of signature verification: " + (signature.verify(signatureBytes) ? "Signature is authenicated." : "WARNING! Signature Mismatch!"));
			
			MainMenu.main(args);
			
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
