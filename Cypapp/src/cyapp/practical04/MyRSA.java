package cyapp.practical04;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.util.Scanner;

import javax.crypto.Cipher;

public class MyRSA {
	public static void main (String[] args) {
		try {
			//get plain text
			Scanner in = new Scanner(System.in);
			System.out.print("Enter a string: ");
			byte[] plainText = in.nextLine().getBytes("UTF8");
			
			//generate keys
			KeyPairGenerator kg = KeyPairGenerator.getInstance("RSA");
			kg.initialize(1024);
			KeyPair key = kg.generateKeyPair();
			
			//create cipher object
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			System.out.println("Provider: " + cipher.getProvider().getInfo());
			
			//encrypt plain text using public key
			cipher.init(Cipher.ENCRYPT_MODE, key.getPublic());
			byte[] cipherText = cipher.doFinal(plainText);
			System.out.println("RSA: ");
			System.out.println("Plain Text: " + new String(plainText));
			System.out.println("Cipher Text: " + new String(cipherText));
			
			//decrypt cipher text using private key
			cipher.init(Cipher.DECRYPT_MODE, key.getPrivate());
			byte[] decryptedPlainText = cipher.doFinal(cipherText);
			System.out.println("Decrypted plain text: " + new String(decryptedPlainText));
			
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
