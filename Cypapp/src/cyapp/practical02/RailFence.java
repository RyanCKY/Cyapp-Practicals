package cyapp.practical02;
import java.util.Scanner;

public class RailFence {
	public static String encrypt (String plainTextString) {
        String output = "";

        for(int i=0 ; i < plainTextString.length() ; i+=2) {
            output += plainTextString.charAt(i);
        }
        for(int i=1 ; i < plainTextString.length() ; i+=2) {
            output += plainTextString.charAt(i);
        }
        
        return output;
	}
	
	public static String decrypt (String cipherTextString, int depth) {
		int r = depth, len = cipherTextString.length();
		  int c = len / depth;
		  char mat[][] = new char[r][c];
		  int k = 0;
		   
		  String plainText = "";
		   
		   
		  for(int i=0;i< r;i++)
		  {
		   for(int j=0;j< c;j++)
		   {
		    mat[i][j]=cipherTextString.charAt(k++);
		   }
		  }
		  for(int i=0;i< c;i++)
		  {
		   for(int j=0;j< r;j++)
		   {
		    plainText+=mat[j][i];
		   }
		  }
		  return plainText;
	}
	
	public static void main (String[] args) {
		Scanner in = new Scanner (System.in);
		
		String plainTextString = " ", cipherTextString;
		int choice;
		
		System.out.println("~RAIL FENCE PROGRAM MAIN MENU~");
		System.out.println("==============================");
		
		System.out.println("|       0. Exit              |");
		System.out.println("|       1. Encrypt Text      |");
		System.out.println("|       2. Decrypt Text      |");
		
		System.out.println("==============================");
		
		do {
			System.out.print("Enter your choice: ");
			choice = in.nextInt();
			
			if (choice == 1 || choice == 2 || choice == 0) {
				if (choice == 1) {
					System.out.print("Enter text to encrypt without spaces: ");
					plainTextString = in.next();
					cipherTextString = encrypt(plainTextString);
					System.out.println("The encrypted text is: " + cipherTextString);
					System.out.println(" ");
				}
				else if (choice == 2) {
					System.out.print("Enter text to decrypt without spaces: ");
					cipherTextString = in.next();
					plainTextString = decrypt(cipherTextString, 2);
					System.out.println("The encrypted text is: " + plainTextString);
					System.out.println(" ");
				}
			}
			else {
				System.out.print("ERROR! Please reset system!");
				System.exit(0);
			}
		} while (choice != 0);
		
		in.close();
	}
}
